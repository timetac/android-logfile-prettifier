'use babel';

import { CompositeDisposable } from 'atom';
import { Point } from 'atom';

export default {

  subscriptions: null,

  activate(state) {

    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'timetac:expandJSONResponse': () => this.expandJSONResponse()
    }));
  },

  deactivate() {
    this.subscriptions.dispose();
  },

  serialize() {
    return {
    };
  },

  expandJSONResponse() {

    let summarizeSingle = function(obj) {
      let summary = "[";
      let results = obj["Results"];
      let deleted = obj["Deleted"];
      let hasResults = results && Array.isArray(results) && results.length > 0;
      let hasDeleted = deleted && Array.isArray(deleted) && deleted.length > 0;
      if (hasResults || hasDeleted) {
        summary += obj["ResourceName"];
        if (hasResults) summary += " results=" + results.length;
        if (hasDeleted) summary += " deleted=" + deleted.length;
      }
      summary += "]";
      return summary;
    }

    let summarize = function(obj) {
      let summary = "";
      let results = obj["Results"];
      if (results !== null) {
        if (results.constructor == Object) {
          summary += "[" + obj["ResourceName"] + " ";
          Object.values(results).forEach(value => {
            summary += summarizeSingle(value);
          })
        } else {
          summary += summarizeSingle(obj);
        }
      }
      return summary;
    }

    let editor
    if (editor = atom.workspace.getActiveTextEditor()) {
      let bufferPos = editor.getCursorBufferPosition();
      let row = bufferPos.row;
      let text = editor.lineTextForBufferRow(bufferPos.row);
      let groups = text.match(/^(.+ .+ .+ )({.+})$/);
      if (groups) {
        try {
          let obj = JSON.parse(groups[2]);
          let summary = summarize(obj);
          let prettified = JSON.stringify(obj, null, 2);
          let range = editor.getBuffer().rangeForRow(bufferPos.row);
          editor.setTextInBufferRange(range, groups[1] + "JSONResponse: " + summary + " " + prettified);
          editor.setCursorBufferPosition(new Point(bufferPos.row, 0));
          editor.moveRight(groups[1].length);
        } catch (error) {
          console.error(error);
        }
      }
    }
  }
};
