# timetac package

This package provides a "Expand JSON Response" command, that operates on JsonRepsonse entries in
log files from our Timetac apps.

To use, place the cursor onto a line of the log file that contains a JsonResponse and run the
"Timetac : Expand JSON Response" commands to expand the JSON and add a summary at the beginning of the line.

Thanks to Atom you can fold/unfold the expanded JSON or you can use UNDO to revert the expansion.

The command can be run from the Packages menu, context menu or via its shortcut Ctrl-Shift-<Plus>.

Installation

Put the timetac folder into ~/.atom/packages folder.
Or (especially when working with cloned repo), clone the repo anywhere you like and afterwards,
go into the timetac folder and call "apm link" from console. This will create a symbolic link
~/.atom/packages/timetac --> your actual folder.

Finally, you need to reload the window (Ctrl-Shift-F5) to activate the plugin.
