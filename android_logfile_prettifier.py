import argparse
import json
import re
import time

parser = argparse.ArgumentParser(description="Prettify android logfile.")
parser.add_argument("logfile", type=str, help="the path to the logfile")
parser.add_argument("outfile", type=str, help="the path for the output", default="outfile", nargs="?")
args = parser.parse_args()
if args.outfile is "outfile":
    args.outfile = args.logfile + ".pretty"


def get_sync_summary(response: dict) -> str:
    ret = ""
    if "ResourceName" in response.keys():
        if response["ResourceName"] == "deltaSync":
            for resource in response["Results"]:
                base_response = response["Results"][resource]
                ret += get_summary_from_baseresponse(base_response)
            return ret
        else:
            return get_summary_from_baseresponse(response)
    return "Unknown"


def get_summary_from_baseresponse(base_response):
    ret = ""
    num_results = len(base_response["Results"])
    num_deleted = 0
    if "Deleted" in base_response:
        num_deleted = len(base_response["Deleted"])

    if num_results != 0:
        ret += base_response["ResourceName"] + ": " + str(num_results) + ", "
        nested_entities = {}
        add_nested_entities(base_response["Results"], nested_entities)
        if len(nested_entities) > 0:
            ret = ret[0:-2] + " NESTED[" + format_nested(nested_entities) + "], "

    if num_deleted != 0:
        ret += base_response["ResourceName"] + " DELETED: " + str(num_deleted) + ", "
    return ret


def add_nested_entities(results: [dict], nested_entities_dict: dict):
    for result in results:
        if "nestedEntities" in result and len(result["nestedEntities"]) > 0:
            for resource in result["nestedEntities"]:
                if resource not in nested_entities_dict:
                    nested_entities_dict[resource] = 0
                nested_entities_dict[resource] += result["nestedEntities"][resource][0]["NumResults"]
                # Add nested NestedEntities if existing
                add_nested_entities(result["nestedEntities"][resource][0]["Results"], nested_entities_dict)


def format_nested(nested_entities: dict):
    ret = ""
    for entity in nested_entities:
        ret += entity + ": " + str(nested_entities[entity]) + ", "
    return ret


start_time = time.time()

with open(args.logfile, "r") as logfile, open(args.outfile, "w") as pretty:
    line = logfile.readline()
    while line != "":
        r = re.search(r"(\[.*\] Network: )(\{.*\})", line)
        if r is not None:
            body = json.loads(r.group(2))
            pretty.write(r.group(1) + "StartResponseBody: " + get_sync_summary(body) + "\n")
            pretty.write(json.dumps(body, indent=4) + "\n")
            pretty.write("EndResponseBody\n")
            line = logfile.readline()
            continue
        pretty.write(line)
        line = logfile.readline()

print("Prettified {0} in {1:.2f} seconds".format(args.outfile, time.time() - start_time))
